# Fibonacci numbers
# 1, 1, 2, 3, 5, 8, 13, 21, ...


def fibonacci(limit):
    nums = []

    current = 0
    next_ = 1

    while current < limit:
        current, next_ = next_, current+next_
        nums.append(current)

    return nums


for n in fibonacci(100):
    print(n, end=', ')
print()


def fibonacci_co(limit):
    current = 0
    next_ = 1

    while current < limit:
        current, next_ = next_, current+next_
        yield current


for n in fibonacci_co(100):
    print(n, end=', ')
