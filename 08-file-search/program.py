import collections
import os


def main():
    """main entry point for the app"""
    print_header()
    directory = get_directory_from_user()
    if not directory:
        print("Sorry we can't search that location")
        return

    text = get_search_text_from_user()
    if not text:
        print("We can't search for nothing!")
        return

    matches = search_directories(directory, text)

    # _matchtext = ' MATCH '
    match_count = 0
    for m in matches:
        match_count += 1
        # print(f'{_matchtext:-^30}')
        # print(f'file: {m.file}')
        # print(f'line: {m.line}')
        # print(f'match: {m.text.strip()}')
        # print()
    print(f'Found {match_count:,} matches')


def print_header():
    num_dashes = 24
    print('-' * num_dashes)
    print('FILE SEARCH APP')
    print('-' * num_dashes)
    print()


def get_directory_from_user():
    directory = input('What directory do you want to search? ')
    if not directory or not directory.strip():
        return None

    if not os.path.isdir(directory):
        return None

    return os.path.abspath(directory)


def get_search_text_from_user():
    text = input('What are you searching for [single phrases only]? ')
    return text.lower()


def search_directories(directory, text):
    # all_matches = []
    items = os.listdir(directory)
    # glob.glob(os.path.join(directory, '*'))

    for item in items:
        itempath = os.path.join(directory, item)
        if os.path.isdir(itempath):
            # matches = search_directories(itempath, text)
            # all_matches.extend(matches)
            # for m in matches:
            #     yield m
            yield from search_directories(itempath, text)
        else:
            yield from search_file(itempath, text)
            # matches = search_file(itempath, text)
            # all_matches.extend(matches)
            # for m in matches:
            #     yield m

    # return all_matches


def search_file(filename, search_text):
    SearchResult = collections.namedtuple(
        'SearchResult',
        ['file', 'line', 'text'],
    )
    # matches = []

    with open(filename, 'r', encoding='utf-8') as f:
        line_number = 0
        for line in f:
            line_number += 1
            # find returns -1 if the substring is not found
            if line.lower().find(search_text) >= 0:
                m = SearchResult(line=line_number, file=filename, text=line)
                yield m


if __name__ == '__main__':
    main()
