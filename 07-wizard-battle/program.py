import random
import time

from actors import Creature, Wizard, SmallAnimal, Dragon


def main():
    """main entry point for the app"""
    print_header()
    game_loop()


def print_header():
    num_dashes = 24
    print('-' * num_dashes)
    print('WIZARD BATTLE APP')
    print('-' * num_dashes)
    print()


def game_loop():

    creatures = [
        SmallAnimal('Toad', 1),
        Creature('Tiger', 12),
        SmallAnimal('Bat', 3),
        Dragon('Dragon', 50, 75, True),
        Wizard('Evil Wizard', 1000),
    ]

    hero = Wizard('Gandalf', 75)

    while True:

        active_creature = random.choice(creatures)
        print(
            f'A {active_creature.name} of level {active_creature.level} has '
            'appeared from a dark and foggy forest...'
        )
        print()
        cmd = input('Do you [a]ttack, [r]un away, or [l]ook around? ')

        if cmd == 'a':
            if hero.attack(active_creature):
                creatures.remove(active_creature)
            else:
                print('The wizard runs and hides, taking time to recover...')
                time.sleep(1)
                print('The wizard returns revitalised!')
        elif cmd == 'r':
            print('The wizard has become unsure of his power and flees!')
        elif cmd == 'l':
            print(f'The wizard {hero.name} takes in the surroundings and sees:')
            for c in creatures:
                print(f' * A {c.name} of level {c.level}')
        else:
            print('OK, exiting game... bye!')
            break

        if not creatures:
            print("You've defeated all the creatures!")


if __name__ == '__main__':
    main()
