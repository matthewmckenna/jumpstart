import requests.exceptions

import movie_svc


def main():
    """main entry point for the app"""
    print_header()
    search_event_loop()


def print_header():
    num_dashes = 24
    print('-' * num_dashes)
    print('MOVIE SEARCH APP')
    print('-' * num_dashes)
    print()


def search_event_loop():
    search = None

    while search != 'x':
        search = input('Movie search text (x to exit): ')
        if search != 'x':
            try:
                results = movie_svc.find_movies(search)
            except requests.exceptions.ConnectionError:
                print('Error: Your network is down')
            except ValueError:
                print('Error: Search text is required')
            except Exception as e:
                print(f'Unexpected error. Details {e}')
            print(f'Found {len(results)} results.')
            for r in results:
                print(f'{r.year} -- {r.title}')
            print()

    print('exiting...')


if __name__ == '__main__':
    main()
