import collections

import requests

MovieResult = collections.namedtuple(
    'MovieResult',
    [
        'imdb_code',
        'title',
        'duration',
        'director',
        'year',
        'rating',
        'imdb_score',
        'keywords',
        'genres',
    ]
)


def find_movies(search_text):
    if not search_text.strip():
        raise ValueError('Search text is required')

    url = f'http://movie_service.talkpython.fm/api/search/{search_text}'

    r = requests.get(url)
    r.raise_for_status()

    movie_data = r.json()
    movie_list = movie_data.get('hits')

    movies = [MovieResult(**m) for m in movie_list]
    movies.sort(key=lambda m: -m.year)

    return movies
