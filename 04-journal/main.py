import journal


def print_header():
    num_dashes = 16
    print('-' * num_dashes)
    print('JOURNAL APP')
    print('-' * num_dashes)


def run_event_loop():
    print('What do you want to do with your journal?')
    cmd = 'truthy value'
    journal_name = 'default'
    journal_data = journal.load(journal_name)

    while cmd and cmd != 'x':
        cmd = input('[L]ist entries, [A]dd an entry, E[x]it: ')
        cmd = cmd.lower().strip()

        if cmd == 'l':
            list_entries(journal_data)
        elif cmd == 'a':
            add_entry(journal_data)
        elif cmd and cmd != 'x':
            print(f"Sorry we don't understand {repr(cmd)}")

    print('Done, goodbye')
    journal.save(journal_name, journal_data)


def list_entries(data):
    print('Your journal entries')
    entries = reversed(data)

    for idx, entry in enumerate(entries, 1):
        print(f'* [{idx}] {entry}')


def add_entry(data):
    text = input('Type your entry, <enter> to exit: ')
    journal.add_entry(text, data)


def main():
    print_header()
    run_event_loop()


if __name__ == '__main__':
    main()
