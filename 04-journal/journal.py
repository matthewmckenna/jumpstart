import os


def load(name):
    data = []
    filename = get_full_pathname(name)

    if os.path.exists(filename):
        with open(filename, 'rt') as f:
            for line in f:
                data.append(line)

    return data


def save(name, journal_data):
    filename = get_full_pathname(name)
    print(f'Saving to {filename}')

    # TODO: directories in `filename` must exist!
    with open(filename, 'wt') as f:
        for entry in journal_data:
            f.write(f'{entry}\n')


def get_full_pathname(name):
    return os.path.abspath(os.path.join('journals', f'{name}.jrnl'))


def add_entry(text, journal_data):
    journal_data.append(text)
