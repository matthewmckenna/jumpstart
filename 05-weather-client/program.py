import collections

import bs4
import requests


def main():
    print_the_header()
    code = input('What zipcode do you want the weather for (e.g., 97201)? ')
    html = get_html_from_web(code)
    report = get_weather_from_html(html)

    print(f'The temp in {report.loc} is {report.temp} {report.scale} and {report.cond}')


def print_the_header():
    """prints a header for the app"""
    num_dashes = 24
    print('-' * num_dashes)
    print('WEATHER APP')
    print('-' * num_dashes)
    print()


def get_html_from_web(zipcode):
    """download the html of the webpage"""
    url = f'https://www.wunderground.com/weather-forecast/{zipcode}'
    r = requests.get(url)

    return r.text


def get_weather_from_html(html):
    """extract weather details from the html"""
    soup = bs4.BeautifulSoup(html, 'html.parser')

    loc = soup.find('div', {'class': 'region-content-header'}).find('h1').get_text()
    condition = soup.find('div', {'class': 'condition-icon'}).get_text()
    temp = (
        soup.find('span', {'class': 'wu-unit-temperature'})
        .find('span', {'class': 'wu-value'})
        .get_text()
    )
    scale = (
        soup.find('span', {'class': 'wu-unit-temperature'})
        .find('span', {'class': 'wu-label'})
        .get_text()
    )

    loc = cleanup_text(loc)
    loc = find_city_and_state_from_location(loc)
    condition = cleanup_text(condition)
    temp = cleanup_text(temp)
    scale = cleanup_text(scale)

    WeatherReport = collections.namedtuple(
        'WeatherReport',
        ['cond', 'temp', 'scale', 'loc'],
    )
    return WeatherReport(cond=condition, temp=temp, scale=scale, loc=loc)


def cleanup_text(text: str) -> str:
    if not text:
        return text

    text = text.strip()

    return text


def find_city_and_state_from_location(loc: str):
    parts = loc.split()
    return parts[0].strip()


if __name__ == '__main__':
    main()
