import os
import shutil

import requests


def get_cat(directory, name):
    url = 'http://consuming-python-services-api.azurewebsites.net/cats/random'
    data = get_data_from_url(url)
    save_image(directory, name, data)


def get_data_from_url(url):
    r = requests.get(url, stream=True)

    # prefer the method below
    # with open(filename, 'wb') as fd:
    #     for chunk in r.iter_content(chunk_size=128):
    #         fd.write(chunk)

    return r.raw


def save_image(directory, name, data):
    filename = os.path.join(directory, f'{name}.jpg')
    with open(filename, 'wb') as f:
        shutil.copyfileobj(data, f)
