import os
import platform
import subprocess

import cat_service


def main():
    print_header()

    directory = get_or_create_output_directory()
    print(directory)

    download_cats(directory)
    display_cats(directory)


def print_header():
    num_dashes = 24
    print('-' * num_dashes)
    print('CAT FACTORY')
    print('-' * num_dashes)


def get_or_create_output_directory():
    base_dir = os.path.abspath(os.path.dirname(__file__))
    directory = 'cat_pictures'
    full_path = os.path.join(base_dir, directory)

    if not os.path.exists(full_path) or not os.path.isdir(full_path):
        print(f'Creating new directory at {full_path}')
        os.mkdir(full_path)

    return full_path


def download_cats(directory):
    cat_count = 8

    print('Contacting server to download cats...')

    for i in range(1, cat_count+1):
        name = f'lolcat_{i}'
        print(f'Downloading {name}')
        cat_service.get_cat(directory, name)

    print('Done')


def display_cats(directory):
    print('Displaying cats in OS window')
    if platform.system() == 'Darwin':
        subprocess.call(['open', directory])
    elif platform.system() == 'Windows':
        subprocess.call(['explorer', directory])
    elif platform.system() == 'Linux':
        subprocess.call(['xdg-open', directory])
    else:
        print(f"We don't understand your platform: {platform.system()}")


if __name__ == '__main__':
    main()
