def mean(data):
    total = 0.0
    for d in data:
        total += d

    return total / max(1, len(data))
