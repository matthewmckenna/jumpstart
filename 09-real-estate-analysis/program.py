import csv
import os
try:
    import statistics
except ImportError:
    import py2_statistics as statistics
from typing import List

from data_types import Purchase


def main():
    """main entry point for the app"""
    print_header()
    filename = get_data_file()
    data = load_file(filename)
    query_data(data)


def print_header():
    """print the header"""
    num_dashes = 24
    print('-' * num_dashes)
    print('REAL ESTATE ANALYSIS APP')
    print('-' * num_dashes)
    print()


def get_data_file():
    base_directory = os.path.abspath(os.path.dirname(__file__))
    return os.path.join(base_directory, 'data', 'SacramentoRealEstateTransactions2008.csv')


def load_file(filename):
    purchases = []
    with open(filename, 'rt', encoding='utf-8') as f:
        reader = csv.DictReader(f)

        for row in reader:
            p = Purchase.create_from_dict(row)
            purchases.append(p)

        return purchases

        # header = f.readline().strip()
        # reader = csv.reader(f)
        # for row in reader:
        #     print(row)


# def load_file_basic(filename):
#     with open(filename, 'rt', encoding='utf-8') as f:
#         header = f.readline().strip()
#         print(f'Found header: {header}')
#         lines = []
#         for line in f:
#             line_data = line.strip().split(',')
#             bed_count = line_data[4]
#             lines.append(line_data)

#         print(lines[:5])


def query_data(data: List[Purchase]):
    # sort the data by price
    data.sort(key=lambda p: p.price)

    high_purchase = data[-1]
    low_purchase = data[0]

    print(
        f'The most expensive house is ${high_purchase.price:,.2f} with '
        f'{high_purchase.beds} beds and {high_purchase.baths} baths.'
    )
    print(
        f'The least expensive house is ${low_purchase.price:,.2f} with '
        f'{low_purchase.beds} beds and {low_purchase.baths} baths.'
    )

    # average house price
    prices = [p.price for p in data]
    avg_price = statistics.mean(prices)
    print(f'The average home price is ${avg_price:,.2f}')

    # average price of 2 bedroom house
    two_bed_homes = (
        p
        for p in data
        if announce(p, f'2-bedrooms, found {p.beds}') and p.beds == 2
    )

    homes = []
    for h in two_bed_homes:
        if len(homes) > 5:
            break
        homes.append(h)

    avg_price_2bed = statistics.mean((announce(p.price, 'price') for p in homes))
    avg_bath_2bed = statistics.mean((p.baths for p in homes))
    avg_sqft_2bed = statistics.mean((p.sq_ft for p in homes))
    print(
        f'Average 2-bedroom home is ${avg_price_2bed:,.2f}, {avg_bath_2bed:.2f} baths, '
        f'{avg_sqft_2bed:.2f} sq_ft'
    )


def announce(item, msg):
    print(f'Pulling item {item} for {msg}')
    return item

if __name__ == '__main__':
    main()
